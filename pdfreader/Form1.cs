﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RasterEdge.XDoc.PDF;
using RasterEdge.XDoc.WindowsViewer;

namespace pdfreader
{
   
    public partial class Form1 : Form
    {
        WinViewer winViewer;
        public int[] totalPages ;
        public Form1()
        {

            InitializeComponent();
            TextBox textBox = new TextBox();
            
            ViewerOptions option = new ViewerOptions();
            option.ShowThumb = false; //Display thumbnail or not, true will display.
            option.ThumbDock = ThumbDock.Right; //Set the location of Thumbnail. (Left, Right, Top, Bottom)
            option.SizeByDocViewer = false; //Spread DocViewer to WinViewer or not, true will not spread.
            option.ShowNavigationBar =false; //Display the navigation or not. It will contains change page to display (first, prev, next, last, zoom in/out).
            option.DocViewerWidth =781;
            option.DocViewerHeight = 377;
            option.ThumbViewerHeight = 377;

            //Create a windows viewer according to the viewer settings.
            winViewer = new WinViewer(option);
            winViewer.Width = 781;
            winViewer.Height = 377;
            //bunifuDropdown1.Items.Add("0/0");
            //bunifuDropdown1.SelectedIndex = 0;
            // bunifuPages1.TabPages.Add("pdf1");
            panel1.Controls.Add(winViewer);
            bunifuDropdown2.SelectedIndex = 7;
            
       
        }

        private void WinViewer_Paint(object sender, PaintEventArgs e)
        {
           // MessageBox.Show(winViewer.GetTotalPageCount().ToString());

        }

        

        
        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
                    }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
      
      
        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "(*.*)|*.*";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.winViewer.LoadFromFile(ofd.FileName);
                panel1.Visible = true;
               
                
            }
        }
        
        private void treeView1_AfterSelect_1(object sender, TreeViewEventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            winViewer.UpPage();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
            winViewer.DownPage();
        }

        private void bunifuHSlider1_Scroll(object sender, Utilities.BunifuSlider.BunifuHScrollBar.ScrollEventArgs e)
        {
            //label2.Text = e.Value.ToString();
            
            winViewer.ZoomIndexChanged(e.Value);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            winViewer.GoToPage(1);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            winViewer.GoToPage(winViewer.GetTotalPageCount());
        }

        private void panel1_VisibleChanged(object sender, EventArgs e)
        {
            
        }
        void Count()
        {
            MessageBox.Show("rr");
            foreach(var num in totalPages)
            {
                Console.WriteLine(num);
            }
            //for (int i = 1; i <= 6; i++)
            //{
            //    //MessageBox.Show(i.ToString());
            //    bunifuDropdown1.Items.Add(totalPages);
            //}
        }
        private void bunifuDropdown1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (bunifuDropdown1.SelectedIndex == -1)
            //{
            //    for (int i = 1; i <= 6; i++)
            //    {
            //        //MessageBox.Show(i.ToString());
            //        bunifuDropdown1.Items.Add(totalPages);
            //    }
            //}
        }
    }
}
