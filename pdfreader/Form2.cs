﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RasterEdge.XDoc.WindowsViewer;

namespace pdfreader
{
    public partial class Form2 : Form
    {
        private WinViewer winViewer;
        WindowsViewerForm viewerForm;
        public Form2()
        {
            InitializeComponent();
            ViewerOptions option = new ViewerOptions();
            Panel panel;
            option.ShowThumb = true; //Display thumbnail or not, true will display.
            option.ThumbDock = ThumbDock.Right; //Set the location of Thumbnail. (Left, Right, Top, Bottom)
            option.SizeByDocViewer = true; //Spread DocViewer to WinViewer or not, true will not spread.
            option.ShowNavigationBar = true; //Display the navigation or not. It will contains change page to display (first, prev, next, last, zoom in/out).
            option.DocViewerWidth = 550;
            option.DocViewerHeight = 320;
            option.ThumbViewerHeight =320;
            
            
            
            
            //Create a windows viewer according to the viewer settings.
            winViewer = new WinViewer(option);
            winViewer.Width = 728;
            winViewer.Height = 394;
            
           
           this.panel1.Controls.Add(winViewer);
           
        }
        
        private void Form2_Load(object sender, EventArgs e)
        {
            
            //WindowsViewerForm windowsViewerForm = new WindowsViewerForm();
            //windowsViewerForm.Controls.Add(winViewer);
            //this.Controls.Add(windowsViewerForm);
  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "(*.*)|*.*";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.winViewer.LoadFromFile(ofd.FileName);
               
               
            }
        }

        private void bunifuVScrollBar1_Scroll(object sender, Bunifu.UI.WinForms.BunifuVScrollBar.ScrollEventArgs e)
        {
            //Graphics g = pictureBox1.CreateGraphics();
            //g.DrawImage(pictureBox1.Image, newRectangle(0, 0, pictureBox1.Height, e.Value));
            
        }
        int value;
        private void bunifuVScrollBar1_Scroll_1(object sender, Bunifu.UI.WinForms.BunifuVScrollBar.ScrollEventArgs e)
        {
             value= bunifuVScrollBar1.Value;
           
     
        }

        private void panel1_Scroll(object sender, ScrollEventArgs e)
        {
            label1.Text = e.NewValue.ToString();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
